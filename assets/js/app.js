// JavaScript Document

/* ************************************************************************************************************************

VICMARO

File:			app.js
Author:			Amapolazul
Homepage:		www.amapolazul.com
Copyright:		2019

************************************************************************************************************************ */

/* Foundation */

$(document).foundation();

/* WOW */

new WOW().init();

/* jQuery */

jQuery.noConflict();

jQuery(document).ready(function () {});