// JavaScript Document

/* ************************************************************************************************************************

VICMARO

File:			Gulpfile.js
Author:			Amapolazul
Homepage:		www.amapolazul.com
Copyright:		2019

************************************************************************************************************************ */

// Requires

var gulp = require('gulp');
var sass = require('gulp-sass');
var rename = require('gulp-rename');
var minifyHTML = require('gulp-htmlmin');
var minifyCSS = require('gulp-minify-css');
var minifyJS = require('gulp-uglify');
var image = require('gulp-imagemin');
var watch = require('gulp-watch');

// HTML

gulp.task('html', function () {
	return gulp
		.src('assets/html/app.html')
		.pipe(minifyHTML({
			collapseWhitespace: true,
			removeComments: true
		}))
		.pipe(rename('index.html'))
		.pipe(gulp.dest(''));
});

// CSS

gulp.task('css', function () {
	return gulp
		.src('assets/css/app.scss')
		.pipe(sass())
		.pipe(minifyCSS())
		.pipe(rename('app.css'))
		.pipe(gulp.dest('build'));
});

// JS

gulp.task('js', function () {
	return gulp
		.src('assets/js/app.js')
		.pipe(minifyJS())
		.pipe(rename('app.js'))
		.pipe(gulp.dest('build'));
});

// Image

gulp.task('image', function () {
	return gulp
		.src('assets/images/*')
		.pipe(image())
		.pipe(gulp.dest('build'));
});

// Watch

gulp.task('watch', function () {
	gulp.watch(['assets/html/**/*.html'], ['html']);
	gulp.watch(['assets/css/**/*.scss'], ['css']);
	gulp.watch(['assets/js/**/*.js'], ['js']);
});

// Default

gulp.task('default', ['html', 'css', 'js', 'image', 'watch']);